package com.cs.bs.es.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author liuwg-a
 * @date 2018/9/16 19:19
 * @description
 */
@Document(indexName = "book", type = "book", shards = 1, replicas = 0, refreshInterval = "-1")
@Data
public class Book {
    private String id;
    private String name;
    private String author;

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
