package com.cs.bs.es.controller;

import com.cs.bs.es.entity.Book;
import com.cs.bs.es.repositories.BookRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dalaoyang
 * @Description
 * @project springboot_learn
 * @package com.dalaoyang.controller
 * @email yangyang@dalaoyang.cn
 * @date 2018/5/4
 */
@RestController
@Api(value = "Elasticsearch Demo", tags = "Elasticsearch Demo")
public class EsDemoController {

    @Autowired
    private BookRepository bookRepository;


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation(value = "都有啥", notes = "")
    public ResponseEntity<Iterable> getAll() {

        Iterable<Book> all = bookRepository.findAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @RequestMapping(value = "/add_index", method = RequestMethod.POST)
    @ApiOperation(value = "添加索引", notes = "")
    public ResponseEntity<String> indexDoc(@RequestBody Book book) {
        System.out.println("book===" + book);
        bookRepository.save(book);
        return new ResponseEntity<>("save executed!", HttpStatus.OK);
    }

    @RequestMapping(value = "/name", method = RequestMethod.GET)
    @ApiOperation(value = "通过书名模糊查找", notes = "")
    public ResponseEntity<List<Book>> getByName(@RequestParam String name) {
        List<Book> booksByName = bookRepository.findBooksByName(name);
        return new ResponseEntity<>(booksByName, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/name", method = RequestMethod.GET)
    @ApiOperation(value = "通过书名精确查找", notes = "")
    public ResponseEntity<List<Book>> findAllByName(@RequestParam String name) {
        List<Book> booksByName = bookRepository.findAllByName(name);
        return new ResponseEntity<>(booksByName, HttpStatus.OK);
    }

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    @ApiOperation(value = "通过参数全文检索", notes = "")
    public ResponseEntity<Iterable<Book>> findAllByQuery(@RequestParam String query) {
        QueryStringQueryBuilder queryBuilder = new QueryStringQueryBuilder(query);

        Iterable<Book> books = bookRepository.search(queryBuilder);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @RequestMapping(value = "/author", method = RequestMethod.GET)
    @ApiOperation(value = "通过作者模糊查找", notes = "")
    public ResponseEntity<List<Book>> getByAuthor(@RequestParam String name) {
        List<Book> booksByName = bookRepository.findBooksByAuthor(name);
        return new ResponseEntity<>(booksByName, HttpStatus.OK);
    }

    @RequestMapping(value = "/id", method = RequestMethod.POST)
    @ApiOperation(value = "通过id删除索引", notes = "")
    public ResponseEntity<String> deleteBook(String id) {
        bookRepository.deleteById(id);
        return new ResponseEntity<>("delete execute!", HttpStatus.OK);
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    @ApiOperation(value = "通过id查找索引", notes = "")
    public ResponseEntity<Book> getBookById(@RequestParam String id) {
        Book book = bookRepository.findBookById(id);
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

}
