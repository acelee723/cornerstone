package com.cs.base.quartz.support;

import lombok.Data;

/**
 * @author wangjiahao
 * @version 1.0
 * @className Option
 * @since 2019-03-25 16:16
 */
@Data
public class Option {

    private String cron;
    private String groupName;
    private String jobClassName;

}
