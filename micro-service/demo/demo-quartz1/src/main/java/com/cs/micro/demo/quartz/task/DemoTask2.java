package com.cs.micro.demo.quartz.task;

import com.cs.base.quartz.support.BaseJob;
import com.cs.base.quartz.support.Option;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoTask
 * @since 2019-03-18 11:25
 */
@Component
public class DemoTask2 extends BaseJob {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("#########demoTask2 run#############");
    }

    @Override
    public Option option() {
        Option option = new Option();
        option.setCron("* * * * * ?");
        option.setGroupName("group1");
        option.setJobClassName(getClass().getTypeName());
        return option;
    }
}
